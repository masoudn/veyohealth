/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.veyodev.veyohealthscribe.persistance.data;

import com.veyodev.veyohealthscribe.persistance.MovescpServerDAO;
import com.veyodev.veyohealthscribe.schema.DicomDevice;
import com.veyodev.veyohealthscribe.schema.MovescpServer;
import java.util.List;
import java.util.UUID;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

/**
 *
 * @author Masoud
 */
public class MovescpDAOImpl implements MovescpServerDAO{
    private MongoTemplate mongo;
    public static final String MOVESCP_COLL = "mvcp";

    public MovescpDAOImpl(MongoTemplate mongo) {
        this.mongo = mongo;
    }
    

    public void add(Object object) {
        MovescpServer server = (MovescpServer)object;
        if(!mongo.collectionExists(MovescpServer.class)){
            mongo.createCollection(MovescpServer.class);
        }
        server.setId(UUID.randomUUID().toString());
        mongo.insert(server,MOVESCP_COLL);
    }

    public void delete(Object object) {
        MovescpServer server = (MovescpServer)object;
        mongo.remove(server, MOVESCP_COLL);
    }

    public void update(Object object) {
        DicomDevice device = new DicomDevice();
        MovescpServer server = (MovescpServer)object;
        Query query = new Query();
        query.addCriteria(Criteria.where("id").is(server.getId()));
        Update update = new Update();
        update.set("scriptDirectory", server.getScriptDirectory());
        update.set("aet", server.getDicomDevice().getAet());
        update.set("port", server.getDicomDevice().getPort());
        update.set("receiver", server.getDicomDevice().isReceiver());
        update.set("sender", server.getDicomDevice().isSender());
        mongo.findAndModify(query, update, MovescpServer.class);
    }

    public Object getById(String id) {
        Query query = new Query();
        query.addCriteria(Criteria.where("id").is(id));
        return mongo.find(query, MovescpServer.class,MOVESCP_COLL );
    }

    public List getAll() {
        return mongo.findAll(MovescpServer.class, MOVESCP_COLL);
    }
    
}
