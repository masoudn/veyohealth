/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.veyodev.veyohealthscribe.persistance.data;

import com.veyodev.veyohealthscribe.persistance.StorescpDAO;
import com.veyodev.veyohealthscribe.schema.DicomDevice;
import com.veyodev.veyohealthscribe.schema.StorescpServer;
import java.util.List;
import java.util.UUID;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

/**
 *
 * @author Masoud
 */
public class StorescpDAOImpl implements StorescpDAO{
    private MongoTemplate mongo;
    public static final String STORESCP_COLL = "stcp";
    

    public void add(Object object) {
        StorescpServer server = (StorescpServer) object;
        if(!mongo.collectionExists(StorescpServer.class)){
            mongo.createCollection(StorescpServer.class);
        }
        server.setId(UUID.randomUUID().toString());
        mongo.insert(server,STORESCP_COLL);
    }

    public void delete(Object object) {
        StorescpServer server = (StorescpServer) object;
        mongo.remove(server, STORESCP_COLL);
    }

    public void update(Object object) {
        DicomDevice device = new DicomDevice();
        StorescpServer server = (StorescpServer) object;
        Query query = new Query();
        query.addCriteria(Criteria.where("id").is(server.getId()));
        Update update = new Update();
        update.set("dicomLocation", server.getDicomLocation());
        update.set("scriptDirectory", server.getScriptDirectory());
        update.set("aet", server.getDicomDevice().getAet());
        update.set("port", server.getDicomDevice().getPort());
        update.set("receiver", server.getDicomDevice().isReceiver());
        update.set("sender", server.getDicomDevice().isSender());
        mongo.findAndModify(query, update, StorescpServer.class);
        
    }

    public Object getById(String id) {
        Query query = new Query();
        query.addCriteria(Criteria.where("id").is(id));
        return mongo.find(query, StorescpServer.class,STORESCP_COLL );
        
    }

    public List<StorescpServer> getAll() {
        return mongo.findAll(StorescpServer.class, STORESCP_COLL);
    }
    
}
