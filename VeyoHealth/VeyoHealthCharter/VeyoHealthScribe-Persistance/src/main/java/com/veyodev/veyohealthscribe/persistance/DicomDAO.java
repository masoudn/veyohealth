/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.veyodev.veyohealthscribe.persistance;

import com.veyodev.veyohealthscribe.schema.DicomDevice;
import com.veyodev.veyohealthscribe.schema.MovescpServer;
import com.veyodev.veyohealthscribe.schema.StorescpServer;
import java.util.List;

/**
 *
 * @author Masoud
 */
public interface DicomDAO extends AbstractDAO{
    List<DicomDevice> getAllOfDicomSenders();
    void addMovescpServer(MovescpServer server);
    void addStorescpServer(StorescpServer server);
    
}
