/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.veyodev.veyohealthscribe.persistance.data;

import com.veyodev.veyohealthscribe.persistance.DicomDAO;
import com.veyodev.veyohealthscribe.schema.DicomDevice;
import com.veyodev.veyohealthscribe.schema.MovescpServer;
import com.veyodev.veyohealthscribe.schema.StorescpServer;
import java.util.List;
import java.util.UUID;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoDbFactory;

/**
 *
 * @author Masoud
 */
public class DicomDaoImpl implements DicomDAO{
    private MongoTemplate jdbc;
    public static final String MOVESCP_COLL = "mscp";
    public static final String STORESCP_COLL = "stcp";

    public DicomDaoImpl(MongoTemplate jdbc) {
        this.jdbc = jdbc;
    }
   

    public List<DicomDevice> getAllOfDicomSenders() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

   
    public void addMovescpServer(MovescpServer server) {
        if(!jdbc.collectionExists(MovescpServer.class)){
            jdbc.createCollection(MovescpServer.class);
        }
        server.setId(UUID.randomUUID().toString());
        jdbc.insert(server,MOVESCP_COLL);      
    }

    public void addStorescpServer(StorescpServer server) {
        if(!jdbc.collectionExists(StorescpServer.class)){
            jdbc.createCollection(StorescpServer.class);
        }
        server.setId(UUID.randomUUID().toString());
        jdbc.insert(server,STORESCP_COLL); 
    }


    public Object getById(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public List getAll() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }  

    public void delete(Object object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void add(Object object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void update(Object object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public Object getById(String id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
