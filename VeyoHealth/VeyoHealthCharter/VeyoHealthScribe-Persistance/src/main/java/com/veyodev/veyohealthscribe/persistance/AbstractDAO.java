/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.veyodev.veyohealthscribe.persistance;

import java.util.List;

/**
 *
 * @author Masoud
 */
public interface AbstractDAO<T> {
    
    void add(T object);
    
    void delete(T object);
    
    void update(T object);
    
    T getById(String id);
    
    List<T> getAll();
    
}
