<%-- 
    Document   : patient
    Created on : May 28, 2013, 2:14:46 PM
    Author     : Masoud
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<!doctype html>
<html lang="en-us">
<head>
	<meta charset="utf-8">
	
	<title>White Label | Wizard</title>
	
	<meta name="description" content="">
	<meta name="author" content="revaxarts.com">
	
	
	<!-- Apple iOS and Android stuff -->
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<link rel="apple-touch-icon-precomposed" href="img/icon.png">
	<link rel="apple-touch-startup-image" href="img/startup.png">
	<meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=no,maximum-scale=1">
	
	<!-- Google Font and style definitions -->
	<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=PT+Sans:regular,bold">
	<link rel="stylesheet" href="css/style.css">
	
	<!-- include the skins (change to dark if you like) -->
	<link rel="stylesheet" href="css/dark/theme.css" id="themestyle">
	<!-- <link rel="stylesheet" href="css/dark/theme.css" id="themestyle"> -->
	
	<!--[if lt IE 9]>
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<link rel="stylesheet" href="css/ie.css">
	<![endif]-->
	
	<!-- Use Google CDN for jQuery and jQuery UI -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.12/jquery-ui.min.js"></script>
	
	<!-- Loading JS Files this way is not recommended! Merge them but keep their order -->
	
	<!-- some basic functions -->
	<script src="js/functions.js"></script>
		
	<!-- all Third Party Plugins -->
	<script src="js/plugins.js"></script>
		
	<!-- all Whitelabel Plugins -->
	<script src="js/wl_Alert.js"></script>
	<script src="js/wl_Breadcrumb.js"></script>
	<script src="js/wl_Form.js"></script>
		
	<!-- configuration to overwrite settings -->
	<script src="js/config.js"></script>
		
	<!-- the script which handles all the access to plugins etc... -->
	<script src="js/wizard.js"></script>
</head>
<body id="wizard">
		<header>
			<div id="logo">
				<a href="login.htm">Veyo Charter</a>
			</div>
		</header>
                <section id="content">
                    <ul class="breadcrumb" data-connect="breadcrumbcontent">
                        <li class="i_house"><a href="#">Welcome</a></li>
                        <li><a href="#">Page2</a></li>
                        <li><a href="#">Page3</a></li>
                        <li><a href="#">Page4</a></li>
                        <li><a href="#">Page5</a></li>
                    </ul>
                    <div id="breadcrumbcontent">
                        <div>
                            <h3>Welcome</h3>
                                <p>
                                    Welcomne Page!!
                                </p>
                                <hr>
                                <div class="fr"><a class="btn small next icon i_arrow_right">Continue</a></div>
                        </div>
                        <div>
                            <h3>Page2</h3>
                            <p>
                                Page2
                            </p>
                            <hr>
                        <div class="fr"><a class="btn small prev icon i_arrow_left">Start</a><a class="btn small next icon i_arrow_right">Summery</a></div>
                        </div>
                        <div>
                                <h3>Page3</h3>
                                <p>
                                        Page3
                                </p>
                                <hr>
                                <div class="fr"><a class="btn small prev icon i_arrow_left">Collecting Information</a><a class="btn small next icon i_arrow_right">Installation</a></div>
                        </div>
                        <div>
                                <h3>Page4</h3>
                                <p>
                                        Page4
                                </p>
                                <hr>
                                <div class="fr"><a class="btn small prev icon i_arrow_left">Summery</a><a class="btn small next icon i_arrow_right">Finish</a></div>
                        </div>
                        <div>
                                <h3>Page5</h3>
                                <p>
                                       Page5
                                </p>
                                <hr>
                                <div class="fr"><a class="btn small" id="loginbtn">Login</a></div>
                        </div>
                </div>
		</section>

		<footer>Copyright by veyodev.com 2013</footer>
		
</body>
</html>
