/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.veyodev.veyohealthscribe.schema;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 *
 * @author Masoud
 */
@Document
public class MovescpServer {
    
    @Id
    private String id;
    private String scriptDirectory;
    private DicomDevice dicomDevice;


    /**
     * @return the scriptDirectory
     */
    public String getScriptDirectory() {
        return scriptDirectory;
    }

    /**
     * @param scriptDirectory the scriptDirectory to set
     */
    public void setScriptDirectory(String scriptDirectory) {
        this.scriptDirectory = scriptDirectory;
    }

    /**
     * @return the dicomDevice
     */
    public DicomDevice getDicomDevice() {
        return dicomDevice;
    }

    /**
     * @param dicomDevice the dicomDevice to set
     */
    public void setDicomDevice(DicomDevice dicomDevice) {
        this.dicomDevice = dicomDevice;
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }
    
    
}
