/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.veyodev.veyohealthscribe.schema;

import org.springframework.data.mongodb.core.mapping.Document;

/**
 *
 * @author Masoud
 */
@Document
public class StorescpServer {
    
    private String id;
    private String dicomLocation;
    private String scriptDirectory;
    private DicomDevice dicomDevice;

    
    /**
     * @return the dicomLocation
     */
    public String getDicomLocation() {
        return dicomLocation;
    }

    /**
     * @param dicomLocation the dicomLocation to set
     */
    public void setDicomLocation(String dicomLocation) {
        this.dicomLocation = dicomLocation;
    }

    /**
     * @return the scriptDirectory
     */
    public String getScriptDirectory() {
        return scriptDirectory;
    }

    /**
     * @param scriptDirectory the scriptDirectory to set
     */
    public void setScriptDirectory(String scriptDirectory) {
        this.scriptDirectory = scriptDirectory;
    }

    /**
     * @return the dicomDevice
     */
    public DicomDevice getDicomDevice() {
        return dicomDevice;
    }

    /**
     * @param dicomDevice the dicomDevice to set
     */
    public void setDicomDevice(DicomDevice dicomDevice) {
        this.dicomDevice = dicomDevice;
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }
    
    
    
}
