/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.veyodev.veyohealthscribe.schema;

import org.springframework.data.mongodb.core.mapping.Document;

/**
 *
 * @author Masoud
 */
@Document
public class DicomDevice {
    
    private int id;
    private String aet;
    private int port;
    private boolean sender;
    private boolean receiver;

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the aet
     */
    public String getAet() {
        return aet;
    }

    /**
     * @param aet the aet to set
     */
    public void setAet(String aet) {
        this.aet = aet;
    }

    /**
     * @return the port
     */
    public int getPort() {
        return port;
    }

    /**
     * @param port the port to set
     */
    public void setPort(int port) {
        this.port = port;
    }

    /**
     * @return the sender
     */
    public boolean isSender() {
        return sender;
    }

    /**
     * @param sender the sender to set
     */
    public void setSender(boolean sender) {
        this.sender = sender;
    }

    /**
     * @return the receiver
     */
    public boolean isReceiver() {
        return receiver;
    }

    /**
     * @param receiver the receiver to set
     */
    public void setReceiver(boolean receiver) {
        this.receiver = receiver;
    }
    
}
